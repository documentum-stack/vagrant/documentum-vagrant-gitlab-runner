#!/bin/bash
#Adding ansible management SSH Keys
if [ ! -e ~/ansible.pub ]
then
    ssh-keygen -q -N '' -f ~/ansible
    mkdir -p ~/.ssh
    cat ansible.pub >> ~/.ssh/authorized_keys
    cat ansible >> ~/.ssh/id_rsa
    chmod 600 ~/.ssh/id_rsa
fi
echo "Private Key of Ansbile is available on /vagrant"
cp -f ~/ansible /vagrant/ansible
cp -f ~/ansible.pub /vagrant/ansible.pub

# Install Git, Docker and Docker Compose
echo "Install Docker"
sudo yum install -y sudo yum-utils git
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Install Python 3.9 and ansible
echo "Install Python and Ansible"
sudo yum remove -y python3
sudo yum install -y python3.9
sudo pip3 install --upgrade pip
sudo pip3 install ansible-core ansible docker

sudo systemctl start docker
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

sudo yum install -y bash-completion
sudo curl -L https://raw.githubusercontent.com/docker/machine/v0.16.0/contrib/completion/bash/docker-machine.bash -o /etc/bash_completion.d/docker-machine

sudo docker volume create gitlab-runner-config
sudo docker run -d --env TZ=GB --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    -v /home/vagrant/ansible:/root/.ssh/id_rsa \
    gitlab/gitlab-runner:latest

sudo docker exec --env RUNNER_TOKEN=${RUNNER_TOKEN} gitlab-runner sh -c 'gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--token $RUNNER_TOKEN \
--executor docker \
--docker-image alpine:latest \
--docker-volumes /var/run/docker.sock:/var/run/docker.sock \
--docker-volumes /home/vagrant/ansible:/root/.ssh/id_rsa \
--description docker-runner'

sudo docker ps

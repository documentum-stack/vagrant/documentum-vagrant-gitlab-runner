# Documentum-Vagrant-Gitlab-Runner

This guide is applicable for Windows-10/11 using Hyper-V. If you are using VirtualBox/VMWare etc. Please feel free to fork and customise accordingly.

## Getting started

To make use of Gitlab Pipeline, it is vital to run on-prem gitlab runner that is configured to interact with other servers on the network. Most of the Pipelines used in this space are ansible driven, hence having SSH integration with the nodes is vital for ansible to work.

Please follow below simple steps to create Gitlab Runner VM on Hyper-V (local/remote)

1. Fork the Project.
2. Enable Hyper-V on Windows-10/11. Please follow official guides.
3. Install Vagrant and Git-Bash. Please follow official guides.
4. Create a Top-Service-Level Gitlab Runner (**Tag**: glr-dev) in GitLab UI and save the token for step 7. This will be used by Ansible Playbooks through GitLab Pipelines.
5. Clone the Project in VS Code.
6. Open Bash Terminal in VS Code.
7. Export Gitlab Runner Token. E.g. `export RUNNER_TOKEN=glrt-xxxxxx`.
8. Run `vagrant up`.
9. Review the logs.

## Releases

- [ ] [V1-CENTOS-7](https://gitlab.com/documentum-stack/vagrant/gitlab-runner-vagrant/-/releases/V1-CENTOS-7)
- [ ] [V1-CENTOS-8](https://gitlab.com/documentum-stack/vagrant/gitlab-runner-vagrant/-/releases/V1-CENTOS-8)

***
